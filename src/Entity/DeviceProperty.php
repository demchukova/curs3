<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DevicePropertyRepository")
 */
class DeviceProperty
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeDevice", inversedBy="deviceProperties")
     */
    private $deviceType;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDeviceType(): ?TypeDevice
    {
        return $this->deviceType;
    }

    public function setDeviceType(?TypeDevice $deviceType): self
    {
        $this->deviceType = $deviceType;

        return $this;
    }
    public function __toString()
    {
        return $this->getTitle();
    }
}

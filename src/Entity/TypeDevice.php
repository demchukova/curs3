<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeDeviceRepository")
 */
class TypeDevice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;
    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $created;
    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DeviceProperty", mappedBy="deviceType")
     */
    private $deviceProperties;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Device", mappedBy="typeDevice", orphanRemoval=true)
     */
    private $devices;

    public function __construct()
    {
        $this->deviceProperties = new ArrayCollection();
        $this->devices = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
    /**
     * Gets triggered only on insert

     */
    public function setCreated()
    {
        $this->created = new \DateTime("now");
    }


    public function getCreated(){
        return $this->created;
    }

    /**
     * Gets triggered every time on update
     */
    public function setUpdated()
    {
        $this->updated = new \DateTime("now");
    }

    public function getUpdated(){
        return $this->updated;
    }

    /**
     * @return Collection|DeviceProperty[]
     */
    public function getDeviceProperties(): Collection
    {
        return $this->deviceProperties;
    }

    public function addDeviceProperty(DeviceProperty $deviceProperty): self
    {
        if (!$this->deviceProperties->contains($deviceProperty)) {
            $this->deviceProperties[] = $deviceProperty;
            $deviceProperty->setDeviceType($this);
        }

        return $this;
    }

    public function removeDeviceProperty(DeviceProperty $deviceProperty): self
    {
        if ($this->deviceProperties->contains($deviceProperty)) {
            $this->deviceProperties->removeElement($deviceProperty);
            // set the owning side to null (unless already changed)
            if ($deviceProperty->getDeviceType() === $this) {
                $deviceProperty->setDeviceType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setTypeDevice($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getTypeDevice() === $this) {
                $device->setTypeDevice(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getTitle();
    }
    
}

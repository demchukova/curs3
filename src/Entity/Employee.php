<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 */
class Employee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Department", inversedBy="employees")
     * @ORM\JoinColumn(nullable=false)
     */
    private $department;

    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $created;
    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Device", mappedBy="employee", orphanRemoval=true)
     */
    private $devices;

    public function __construct()
    {
        $this->devices = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }
    public function setCreated()
    {
        $this->created = new \DateTime("now");
    }


    public function getCreated(){
        return $this->created;
    }

    /**
     * Gets triggered every time on update
     */
    public function setUpdated()
    {
        $this->updated = new \DateTime("now");
    }

    public function getUpdated(){
        return $this->updated;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setEmployee($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getEmployee() === $this) {
                $device->setEmployee(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getFirstName();
    }


}

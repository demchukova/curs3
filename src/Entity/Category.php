<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable = false)
     */
    private $updated;

//    /**
//     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="category")
//     * @ORM\JoinColumn(nullable=false)
//     */
//    private $product;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(): self
    {

        $this->created = new \DateTime("now");

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(): self
    {

        $this->updated = new \DateTime("now");

        return $this;
    }

//    public function getProduct(): ?Product
//    {
//        return $this->product;
//    }
//
//    public function setProduct(?Product $product): self
//    {
//        $this->product = $product;
//
//        return $this;
//    }
}

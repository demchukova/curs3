<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Author;
use Symfony\Component\Form\FormTypeInterface;
/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @ORM\Entity
 */

class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Author",
     *      cascade ={"persist"},
     *     inversedBy="articles")
     * @ORM\JoinColumn(name= "author_id", nullable=false, onDelete="CASCADE")
     */
    private $author;


    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor(Author $author)
    {
        $this->author = $author;
    }



    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated()
    {
        $this->created = new \DateTime("now");

    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated()
    {
        $this->updated = new \DateTime("now");

    }

    public function __toString()
    {
      return $this->getTitle();
    }
}

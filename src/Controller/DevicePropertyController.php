<?php

namespace App\Controller;

use App\Entity\TypeDevice;

use App\Entity\DeviceProperty;
use App\Form\DevicePropertyType;
use App\Repository\DevicePropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Author;

/**
 * @Route("/device/property")
 */
class DevicePropertyController extends Controller
{
    /**
     * @Route("/", name="device_property_index", methods="GET")
     */
    public function index(DevicePropertyRepository $devicePropertyRepository): Response
    {
        return $this->render('device_property/index.html.twig', ['device_properties' => $devicePropertyRepository->findAll()]);
    }

    /**
     * @Route("/new", name="device_property_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $typeDevice = $entityManager->getRepository(TypeDevice::class)->findAll();


        $deviceProperty = new DeviceProperty();


        $form = $this->createForm(DevicePropertyType::class, null,
            [
                'deviceType' => $typeDevice,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('device_property_index');
        }

        return $this->render('device_property/new.html.twig', [
            'device_property' => $deviceProperty,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="device_property_show", methods="GET")
     */
    public function show(DeviceProperty $deviceProperty): Response
    {
        return $this->render('device_property/show.html.twig', ['device_property' => $deviceProperty]);
    }

    /**
     * @Route("/{id}/edit", name="device_property_edit", methods="GET|POST")
     */
    public function edit(Request $request, DeviceProperty $deviceProperty): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $typeDevice = $entityManager->getRepository(TypeDevice::class)->findAll();


        $form = $this->createForm(DevicePropertyType::class, $deviceProperty,
            [
                'deviceType' => $typeDevice,
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('device_property_edit', ['id' => $deviceProperty->getId()]);
        }

        return $this->render('device_property/edit.html.twig', [
            'device_property' => $deviceProperty,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="device_property_delete", methods="DELETE")
     */
    public function delete(Request $request, DeviceProperty $deviceProperty): Response
    {
        if ($this->isCsrfTokenValid('delete'.$deviceProperty->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($deviceProperty);
            $em->flush();
        }

        return $this->redirectToRoute('device_property_index');
    }
}

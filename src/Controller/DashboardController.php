<?php
/**
 * Created by PhpStorm.
 * User: sdemchukova
 * Date: 4/16/18
 * Time: 2:16 AM
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\Task;
use App\Repository\ArticleRepository;
use App\Repository\TaskRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class DashboardController extends Controller
{

    /**
     * @Route("/dashboard", name="dashboard", methods="GET")
     */
    public function index(ArticleRepository $articles, TaskRepository $tasks): Response
    {
        return $this->render('dashboard/index.html.twig',
            ['articles' => $articles->findFirst(3),
                'tasks'=> $tasks->findFirst(5)
            ]
        );
    }




}
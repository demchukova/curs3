<?php
/**
 * Created by PhpStorm.
 * User: sdemchukova
 * Date: 3/21/18
 * Time: 10:44 PM
 */

namespace App\Controller;


use App\Entity\Author;
use App\Form\AuthorType;

use PhpParser\Node\Stmt\Return_;
use Simple\ProfileBundle\Form\Type\ChooseAuthorType;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class AuthorController extends Controller
{
    /**
     * @Route("/author/create", name="author_create")
     */
    public function createAction(Request $request)
    {
        $author = new Author();
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $author->onPrePersist();


            $entityManager->flush();

            return $this->render('author/succes.html.twig', [
                'authorLastName' => $author->getLastName(),
                'authorFirstName' => $author->getFirstName()
            ]);
        }
        return $this->render('author/create.html.twig', [
            'authorForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/author/{id}/edit", name="author_edit")
     */
    public function editAction(Request $request, string $id)
    {
        $author = $this->getDoctrine()
            ->getRepository(Author::class)
            ->find($id);

        $form = $this->createForm(
            AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $author->onPreUpdate();
            $entityManager->flush();

        }

        return $this->render('author/edit.html.twig', [
            'authorEditForm' => $form->createView()
        ]);
    }

    /**
     * @Route("author/list", name="author_list")
     */
    public function listAction()
    {
        $authors = $this->getDoctrine()
            ->getRepository(Author::class)
            ->findAll();
        return $this->render('author/list.html.twig', ['authors' => $authors]);

    }

    /**
     * @Route("author/{id}/show", name="author_show")
     */
    public function showAction(string $id)
    {
        $author = $this->getDoctrine()
            ->getRepository(Author::class)
            ->find($id);
        return $this->render('author/show.html.twig', ['author' => $author]);

    }

    /**
     * @param string $id
     * @Route("author/{id}/delete", name="author_delete")
     *  @return Response
     */
    public function deleteAction(string $id)
    {

        try {
            $entityManager = $this->getDoctrine()->getManager();
            $author = $entityManager->getRepository(Author::class)->find($id);


            $entityManager->remove($author);
            $entityManager->flush();
            return $this->redirectToRoute('author_index');
    ;
        } catch (Exception $e) {
            return Response::HTTP_BAD_REQUEST;

        }
    }


}
<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\Invoice;
use App\Entity\Employee;
use App\Entity\Manufacturer;
use App\Entity\State;
use App\Entity\TypeDevice;
use App\Form\DeviceType;
use App\Repository\DeviceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/device")
 */
class DeviceController extends Controller
{
    /**
     * @Route("/", name="device_index", methods="GET")
     */
    public function index(DeviceRepository $deviceRepository): Response
    {
        return $this->render('device/index.html.twig', ['devices' => $deviceRepository->findAll()]);
    }

    /**
     * @Route("/new", name="device_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $device = new Device();

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(DeviceType::class, null,
           array(
               'invoice' => $em->getRepository(Invoice::class)->findAll() ,
               'manufacturer'=> $em->getRepository(Manufacturer::class)->findAll(),
               'employee' =>$em->getRepository(Employee::class)->findAll(),
               'state' => $em->getRepository(State::class)->findAll(),
               'typeDevice' =>$em->getRepository(TypeDevice::class)->findAll(),
           )
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $data->setCreated();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('device_index');
        }

        return $this->render('device/new.html.twig', [
            'device' => $device,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="device_show", methods="GET")
     */
    public function show(Device $device): Response
    {
        return $this->render('device/show.html.twig', ['device' => $device]);
    }

    /**
     * @Route("/{id}/edit", name="device_edit", methods="GET|POST")
     */
    public function edit(Request $request, Device $device): Response
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(DeviceType::class, $device,
            array(
                'invoice' => $em->getRepository(Invoice::class)->findAll() ,
                'manufacturer'=> $em->getRepository(Manufacturer::class)->findAll(),
                'employee' =>$em->getRepository(Employee::class)->findAll(),
                'state' => $em->getRepository(State::class)->findAll(),
                'typeDevice' =>$em->getRepository(TypeDevice::class)->findAll(),
            ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $device->setUpdated();
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('device_edit', ['id' => $device->getId()]);
        }

        return $this->render('device/edit.html.twig', [
            'device' => $device,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="device_delete", methods="DELETE")
     */
    public function delete(Request $request, Device $device): Response
    {
        if ($this->isCsrfTokenValid('delete'.$device->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($device);
            $em->flush();
        }

        return $this->redirectToRoute('device_index');
    }

}

<?php

namespace App\Controller;

use App\Entity\Author;
use App\Form\AuthorType;

use App\Repository\ArticleRepository;

use App\Repository\TaskRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class BlogController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(ArticleRepository $articles, TaskRepository $tasks): Response
    {
        return $this->render('dashboard/index.html.twig',
            ['articles' => $articles->findFirst(3),
                'tasks'=> $tasks->findFirst(5)
            ]
        );
    }
    /**
     *
     * @Route("/blog/author/{id}", name="author.show")
     */
    public function showAction($id)
    {
        $author = $this->getDoctrine()
            ->getRepository(Author::class)
            ->find($id);

        if (!$author) {
            throw $this->createNotFoundException(
                'No author found for id ' . $id
            );
        }

        return new Response('Check out this great product: ' . $author->getLastName());

        // or render a template
        // in the template, print things with {{ product.name }}
        // return $this->render('product/show.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/blog/author/edit/{id}", name="author.edit")
     */
    public function updateAction($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $author = $entityManager->getRepository(Author::class)->find($id);

        if (!$author) {
            throw $this->createNotFoundException(
                'No Author found for id ' . $id
            );
        }

        $author->setLastName('New author name!');
        $author->onPreUpdate();
        $entityManager->flush();

        return $this->redirectToRoute('author.show', [
            'id' => $author->getId()
        ]);
    }

    /**
     * @Route("/blog/author/greater/{id}", name="author.show.greater")
     */
    public function showGreaterAuthors($id)
    {
        $authors = $this->getDoctrine()
            ->getRepository(Author::class)
            ->findAllGreaterThanId($id);
        return $this->render('articles/show.html.twig', ['authors' => $authors]);
    }

    /////////Actions with form
    ///

    /**
     * @Route("/author/create", name="author.create")
     */
    public function createAuthor(Request $request)
    {
        $author = new Author();
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $author->onPrePersist();


            $entityManager->flush();

            return $this->render('form/succes.html.twig', [
                'authorLastName' => $author->getLastName(),
                'authorFirstName'=> $author->getFirstName()
            ]);
            return new Response('Check out this great product: ' . $author->getLastName());
        }
        return $this->render('form/create.html.twig', [
            'authorForm' => $form->createView()
        ]);
    }


}

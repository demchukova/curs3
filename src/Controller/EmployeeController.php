<?php

namespace App\Controller;

use App\Entity\Department;
use App\Entity\Employee;
use App\Entity\Device;
use App\Form\EmployeeType;
use App\Repository\DeviceRepository;

use App\Repository\EmployeeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/employee")
 */
class EmployeeController extends Controller
{
    /**
     * @Route("/", name="employee_index", methods="GET")
     */
    public function index(EmployeeRepository $employeeRepository): Response
    {
        return $this->render('employee/index.html.twig', ['employees' => $employeeRepository->findAll()]);
    }

    /**
     * @Route("/new", name="employee_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $employee = new Employee();

        $entityManager = $this->getDoctrine()->getManager();
        $departments = $entityManager->getRepository(Department::class)->findAll();

        $form = $this->createForm(EmployeeType::class, null,

            [
                'departments' => $departments,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $data ->setCreated();
            $em->persist($data);

            $em->flush();

            return $this->redirectToRoute('employee_index');
        }

        return $this->render('employee/new.html.twig', [
            'employee' => $employee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="employee_show", methods="GET")
     */
    public function show(Employee $employee): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $devices = $entityManager->getRepository(Device::class)->findBy(['employee'=>$employee->getId()]);
        dump($devices);
        return $this->render('employee/show.html.twig', [
            'employee' => $employee,
            'devices' => $devices
            ]);
    }

    /**
     * @Route("/{id}/edit", name="employee_edit", methods="GET|POST")
     */
    public function edit(Request $request, Employee $employee): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        $departments = $entityManager->getRepository(Department::class)->findAll();

        $form = $this->createForm(EmployeeType::class, $employee,

            [
                'departments' => $departments,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $employee->setUpdated();
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('employee_edit', ['id' => $employee->getId()]);
        }

        return $this->render('employee/edit.html.twig', [
            'employee' => $employee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="employee_delete", methods="DELETE")
     */
    public function delete(Request $request, Employee $employee): Response
    {
        if ($this->isCsrfTokenValid('delete'.$employee->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($employee);
            $em->flush();
        }

        return $this->redirectToRoute('employee_index');
    }
}

<?php

namespace App\Controller;

use App\Entity\TypeDevice;
use App\Form\TypeDeviceType;
use App\Repository\TypeDeviceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/device")
 */
class TypeDeviceController extends Controller
{
    /**
     * @Route("/", name="type_device_index", methods="GET")
     */
    public function index(TypeDeviceRepository $typeDeviceRepository): Response
    {
        return $this->render('type_device/index.html.twig', ['type_devices' => $typeDeviceRepository->findAll()]);
    }

    /**
     * @Route("/new", name="type_device_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $typeDevice = new TypeDevice();
        $form = $this->createForm(TypeDeviceType::class, $typeDevice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $typeDevice->setCreated();
            $em->persist($typeDevice);
            $em->flush();

            return $this->redirectToRoute('type_device_index');
        }

        return $this->render('type_device/new.html.twig', [
            'type_device' => $typeDevice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_device_show", methods="GET")
     */
    public function show(TypeDevice $typeDevice): Response
    {
        return $this->render('type_device/show.html.twig', ['type_device' => $typeDevice]);
    }

    /**
     * @Route("/{id}/edit", name="type_device_edit", methods="GET|POST")
     */
    public function edit(Request $request, TypeDevice $typeDevice): Response
    {
        $form = $this->createForm(TypeDeviceType::class, $typeDevice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeDevice->setUpdated();
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_device_edit', ['id' => $typeDevice->getId()]);
        }

        return $this->render('type_device/edit.html.twig', [
            'type_device' => $typeDevice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_device_delete", methods="DELETE")
     */
    public function delete(Request $request, TypeDevice $typeDevice): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeDevice->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeDevice);
            $em->flush();
        }

        return $this->redirectToRoute('type_device_index');
    }
}

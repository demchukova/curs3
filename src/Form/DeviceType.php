<?php

namespace App\Form;

use App\Entity\Device;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'typeDevice',
                ChoiceType::class,
                [
                    'label'    => "Choose device type",
                    'choices'  => $options['typeDevice'],
                    'choice_label' => function($options) {
                        return $options->getTitle();
                    },
                ])
            ->add('manufacturer',
                ChoiceType::class,
                [
                    'label'    => "Choose manufacturer",
                    'choices'  => $options['manufacturer'],
                    'choice_label' => function($options) {
                        return $options->getTitle();
                    },
                ])
            ->add('model')
            ->add('price')
            ->add('invoice',
                ChoiceType::class,
                [
                    'label'    => "Choose invoice",
                    'choices'  => $options['invoice'],
                    'choice_label' => function($options) {
                        return $options->getTitle();
                    },
                ])
            ->add('state',
                ChoiceType::class,
                [
                    'label'    => "Choose state",
                    'choices'  => $options['state'],
                    'choice_label' => function($options) {
                        return $options->getTitle();
                    },
                ])
            ->add('employee',
                ChoiceType::class,
                [
                    'label'    => "Choose employee",
                    'choices'  => $options['employee'],
                    'choice_label' => function($options) {
                        return $options->getFirstName()." ".$options->getLastName();
                    },
                ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'invoice' => [],
            'manufacturer'=> [],
            'employee' =>[],
            'state' => [],
            'typeDevice' =>[],
            'data_class' => Device::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\DeviceProperty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DevicePropertyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('deviceType',
                ChoiceType::class,
                [
                    'label'    => "Choose device type",
                    'choices'  => $options['deviceType'],
                    'choice_label' => function($options) {
                        return $options->getTitle();
                    },
                ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'deviceType'=>[],
            'data_class' => DeviceProperty::class,
        ]);
    }
}

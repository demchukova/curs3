<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Author;
use App\Repository\AuthorRepository;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'author',
                ChoiceType::class,
                [
                    'label'    => "Choose author",
                    'choices'  => $options['authors'],
                    'choice_label' => function($options) {
                        return $options->getLastName()." ".$options->getFirstName();
                    },
    ])
            ->add('title')
            ->add('description')
            ->add('content');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'authors' =>[],
            'data_class' => Article::class,
        ]);
    }
}

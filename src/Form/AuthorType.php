<?php
/**
 * Created by PhpStorm.
 * User: sdemchukova
 * Date: 3/20/18
 * Time: 10:47 PM
 */

namespace App\Form;


use App\Entity\Author;
use App\Repository\AuthorRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class AuthorType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastName')
            ->add('firstName')
            ->add('comment', TextareaType::class)

//            ->add('created', DateTimeType::class)
//            ->add('save', SubmitType::class, ['label' => 'Create Author'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => Author::class,

        ]);


    }

    /**
     * @param Request $request
     */
    public function new(Request $request)
    {
        $author = new Author();
        $form = $this->createForm(AuthorType::class, $author);

        // ...
    }
}
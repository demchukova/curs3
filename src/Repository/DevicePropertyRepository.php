<?php

namespace App\Repository;

use App\Entity\DeviceProperty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DeviceProperty|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeviceProperty|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeviceProperty[]    findAll()
 * @method DeviceProperty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DevicePropertyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DeviceProperty::class);
    }

//    /**
//     * @return DeviceProperty[] Returns an array of DeviceProperty objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeviceProperty
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

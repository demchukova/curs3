<?php

namespace App\Repository;

use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Author::class);
    }

    public function findAllGreaterThanId($id): array
    {
        // automatically knows to select Authors
        // the "a" is an alias you'll use in the rest of the query
        $qb = $this->createQueryBuilder('a')
            ->andWhere('a.id > :id')
            ->setParameter('id', $id)
            ->orderBy('a.id', 'ASC')
            ->getQuery();

        return $qb->execute();

        // to get just one result:
        // $product = $qb->setMaxResults(1)->getOneOrNullResult();
    }
//    public function findAuthors(){
//        return $this->createQueryBuilder('a')
//            ->orderBy('a.lastName', 'ASC');
//
//    }
    public function findAuthors(){
        $qb = $this->createQueryBuilder('a')
//            ->select('a.lastName')
            ->select((array('partial a.{id, lastName, firstName}')))
            ->orderBy('a.lastName', 'DESC')
            ->getQuery();

        return $qb->execute();

    }

//    public function findById($id)
//    {
//        // automatically knows to select Authors
//        // the "a" is an alias you'll use in the rest of the query
//        $qb = $this->createQueryBuilder('a')
//            ->andWhere('a.id = :id')
//            ->setParameter('id', $id)
//            ->getQuery();
//
//        return $qb->execute();
//
//        // to get just one result:
//        // $product = $qb->setMaxResults(1)->getOneOrNullResult();
//    }


//    /**
//     * @return Author[] Returns an array of Author objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Author
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
